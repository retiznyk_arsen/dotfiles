# arandr-generated script to set dual monitor layout
~/.screenlayout/dual_monitor.sh &      

# mini-driver-script to make CapsLock into LMB
~/Documents/capslmb.py &	
setxkbmap -option caps:none	

# apply ICC profiles to monitors
xiccd &	

# allows to bind Openbox menu (or anything else) to Win key without breaking Win+Something keybinds.
xcape -e 'Super_L=Control_L|Alt_L|space'

# self-explanatory
clipit &
tint2 &
nitrogen --restore &
setxkbmap -layout "us,ru" -option "grp:win_space_toggle"
dunst &
xfce4-power-manager &
sleep 1 && redshift -l 53:50 -t 6500:4500 &
xset r rate 350 25 &
sleep 1 && /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
cantata &
xbindkeys -p &
~/.local/bin/appimaged &
orage &


