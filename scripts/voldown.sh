#!/bin/sh
pactl set-sink-mute 0 false ; pactl set-sink-volume 0 -2%
notify-send $(pactl list sinks | grep '^[[:space:]]Volume:' | head -n $(( $SINK + 1 )) | tail -n 1 | sed -e 's,.* \([0-9][0-9]*\)%.*,\1,' | awk '{ printf "Volume: %s%", $o}')